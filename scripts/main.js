
(function(){

	var app = angular.module('app', [ ]);

	//Get HTML DOM
	var ctx = document.getElementById("myChart").getContext("2d");
	var myChart = document.getElementById("myChart");

	//Load the data from API
	var reportAPI = reportAPIService();

	//Plot the line to Line Graph
	var myNewChart = new Chart(ctx).Line(reportAPI.data);

	//Drill Down to Details
	var reportLogicImpl = new reportLogic();

	myChart.onclick = function(evt){
		reportLogicImpl.drillDown(evt, myNewChart);
		myNewChart.destroy();
		var ctx2 = document.getElementById("myChart").getContext("2d");
		myNewChart = new Chart(ctx2).Pie(reportAPI.pieData);
	};

	// Start - Angular Controllers 
	app.controller('CommentController', function(){
		this.comments = reportAPI.getComments();
		this.comment = {};

		this.addComment = function(){
			reportAPI.addComment(this.comment);
			this.comment = {};
		};
	});
	// End - Angular Controllers

})();