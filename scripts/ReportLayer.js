var reportLogic = (function(){

	//Event function for clicking points in chart
	var drillDown = function(evt, myLineChart){
		var activePoints = myLineChart.getPointsAtEvent(evt);

		alert('Drill Down of report for the month of ' + activePoints[0].label);
	};

	return{
		drillDown : drillDown
	}
});