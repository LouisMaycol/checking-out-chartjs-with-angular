var reportAPIService = (function(){

	//Just created a dummy data to plot reports for Demo. The API Call should be done here
	var data = {

			//this defines the x-coordinate of the chart
		    labels: ["January", "February", "March", "April", "May", "June", "July"],

		    //data is the point inside the chart
		    //datasets defines the looks in the chart, in this case, this defines the ui for line chart
		    datasets: [
		        {
		            label: "Sales dataset",
		            fillColor: "rgba(220,220,220,0.2)",
		            strokeColor: "rgba(220,220,220,1)",
		            pointColor: "rgba(220,220,220,1)",
		            pointStrokeColor: "#fff",
		            pointHighlightFill: "#fff",
		            pointHighlightStroke: "rgba(220,220,220,1)",
		            data: [300000, 450000, 350000, 300000, 500000, 450000, 400000]
		        }	       
		    ]
	};

	var pieData = [
	    {
	        value: 300,
	        color:"#F7464A",
	        highlight: "#FF5A5E",
	        label: "1st week"
	    },
	    {
	        value: 50,
	        color: "#46BFBD",
	        highlight: "#5AD3D1",
	        label: "2nd week"
	    },
	    {
	        value: 100,
	        color: "#FDB45C",
	        highlight: "#FFC870",
	        label: "3rd week"
	    },
	    {
	    	value: 200,
	        color: "#0000FF",
	        highlight: "#6666FF",
	        label: "4th week"
	    }
	]

	//comments data
	var commentsList = [
		{
			comment: "The sales is good",
			author: "LMConcepcion@outlook.com"
		},
		{
			comment: "This is great!",
			author: "My@boss.com"
		}
	];

	var getComments = function(){
		return commentsList;
	}

	var addComment = function(comment){
		commentsList.push(comment);
	}

	return {
		data : data,
		getComments : getComments,
		addComment : addComment,
		pieData : pieData
	};	
});

	//In case that we are going to use the data from an Actual API, this code will be done
/*
	var reportAPIService = (function(){
		
		var labels = [];
		var sales = [];

		var reportAPI = new reportAPICall();
		var reportData = reportAPI.getReport();

		reportData.done(function(data){
			labels = data.labels;
			sales = data.sales;
		});

		var data = {

			    labels: labels,
			    datasets: [
			        {
			            label: "My Dataset",
			            fillColor: "rgba(220,220,220,0.2)",
			            strokeColor: "rgba(220,220,220,1)",
			            pointColor: "rgba(220,220,220,1)",
			            pointStrokeColor: "#fff",
			            pointHighlightFill: "#fff",
			            pointHighlightStroke: "rgba(220,220,220,1)",
			            data: sales
			        }	       
			    ]
		};

		return {
			data : data
		}

	});
	
*/
/*
	This following line of code will come from an API Hosted in IIS Server
*/

/*
	var reportAPI = (function(){

		var getReport = function(){
			return $.get('/Report/GenerateReport');
		}

		return{
			getReport : getReport
		};

	});
*/